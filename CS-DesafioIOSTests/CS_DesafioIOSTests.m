//
//  CS_DesafioIOSTests.m
//  CS-DesafioIOSTests
//
//  Created by Luiz Cesar Lopes on 27/09/16.
//  Copyright © 2016 Jorge Luis. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RepositorioDAO.h"
#import "PullRequestDAO.h"
@interface CS_DesafioIOSTests : XCTestCase
@property (strong,nonatomic) RepositorioDAO * repositorioDAO;
@end

@implementation CS_DesafioIOSTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.repositorioDAO = [[RepositorioDAO alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testIncrementarPaginaRepositorios{
    XCTestExpectation * carregouRepositorios = [self expectationWithDescription:@"Carregar Repositorios na Primeira Incrementacao"];
    [self.repositorioDAO incrementarPaginaComFuncaoCallback:^{
        [carregouRepositorios fulfill];
    }];
    [self waitForExpectationsWithTimeout:3.0 handler:^(NSError * _Nullable error) {
        NSLog(@"Nao conseguiu carregar os repositorios! %@",error);
    }];
}

-(void)testNumeroPaginasRepositorios{
    [self testIncrementarPaginaRepositorios];
    NSInteger numeroDePaginas = [self.repositorioDAO numeroDePaginasEmMemoria];
    XCTAssertNotEqual(numeroDePaginas, 0);
    XCTAssertEqual(numeroDePaginas, 1);
}
-(void)testIncrementarPaginaRepositoriosNovamente{
    [self testIncrementarPaginaRepositorios];
    XCTestExpectation * carregouRepositorios = [self expectationWithDescription:@"Carregar Repositorios na Segunda Incrementacao"];
    [self.repositorioDAO incrementarPaginaComFuncaoCallback:^{
        [carregouRepositorios fulfill];
    }];
    [self waitForExpectationsWithTimeout:3.0 handler:^(NSError * _Nullable error) {
        NSLog(@"Nao conseguiu carregar os repositorios! %@",error);
    }];
}

-(void)testNumeroPaginasRepositoriosNovamente{
    [self testIncrementarPaginaRepositoriosNovamente];
    NSInteger numeroDePaginas = [self.repositorioDAO numeroDePaginasEmMemoria];
    XCTAssertNotEqual(numeroDePaginas, 1);
    XCTAssertEqual(numeroDePaginas,2);
}

-(void)testNumeroRepositoriosEmMemoria{
    [self testNumeroPaginasRepositorios];
    NSInteger repositoriosEmMemoria = [self.repositorioDAO numeroDeRepositoriosPorPagina]*[self.repositorioDAO numeroDePaginasEmMemoria];
    XCTAssertEqual(repositoriosEmMemoria, 20);
    XCTAssertEqual([self.repositorioDAO numeroDeRepositoriosEmMemoria], repositoriosEmMemoria);
    XCTAssertEqual([self.repositorioDAO numeroDeRepositoriosEmMemoria], 20);

}

-(void)testRelacaoEntreRepositorioEIndexPath{
    [self testNumeroRepositoriosEmMemoria];
    NSIndexPath * indexPrimeiroRepositorio = [NSIndexPath indexPathForRow:0 inSection:0];
    Repositorio * primeiroRepositorio = [self.repositorioDAO repositorioDoIndexPath:indexPrimeiroRepositorio];
    XCTAssertEqual(indexPrimeiroRepositorio, [self.repositorioDAO indexDoRepositorio:primeiroRepositorio]);
}

@end
