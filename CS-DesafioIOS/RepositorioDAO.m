//
//  RepositorioDAO.m
//  Estudo4
//
//  Created by Luiz Cesar Lopes on 27/09/16.
//  Copyright © 2016 Luis Cesar. All rights reserved.
//

#import "RepositorioDAO.h"
static const NSString * enderecoBaseAPI = @"https://api.github.com/search/repositories?q=language:Java&sort=stars";
static const NSInteger  per_page=20;
@interface RepositorioDAO()
@property (assign) NSInteger paginas; //O numero de paginas recuperadas da API, assim como a pagina atual e tambem a ultima pagina(section) sendo mostrada no tableDelegate
@end

@implementation RepositorioDAO


#pragma mark Metodos Construtores
-(id)init{
    self = [super init];
    if(self){
        _paginas = 0;
        _paginasRepositorios = [[NSMutableArray alloc] init];
        NSArray * mapeamentoAtributos = @[
                                          [DCObjectMapping mapKeyPath:@"owner" toAttribute:@"dono" onClass:[Repositorio class]],
                                          [DCObjectMapping mapKeyPath:@"description" toAttribute:@"descricaoRepositorio" onClass:[Repositorio class]],
                                          [DCObjectMapping mapKeyPath:@"name" toAttribute:@"nomeRepositorio" onClass:[Repositorio class]],
                                          [DCObjectMapping mapKeyPath:@"stargazers_count" toAttribute:@"estrelas" onClass:[Repositorio class]]
                                          ];
        [self.configuracaoJsonParser addObjectMappings:mapeamentoAtributos];
    }
    return self;
}

-(id)initWithTableDelegate:(UITableView *)tableDelegate{
    self = [self init];
    if(self){
        self.tableDelegate = tableDelegate;
    }
    return self;
}

#pragma marks Metodos da Instancia

-(Repositorio *)repositorioDoIndexPath:(NSIndexPath *)indexPath{
    NSInteger index = indexPath.row%per_page;
    NSInteger page = indexPath.row/per_page;
    __weak NSMutableArray * repositorios = [self.paginasRepositorios objectAtIndex:page];
    __weak Repositorio * repositorio;
    repositorio =[repositorios objectAtIndex:index];
    
    return repositorio;
}

-(NSIndexPath *)indexDoRepositorio:(Repositorio *)repositorio{
    NSInteger row=0;
    NSInteger section=0;
    for(NSMutableArray * arrayRepositorios in self.paginasRepositorios){
        if([arrayRepositorios containsObject:repositorio]){
            row+= [arrayRepositorios indexOfObject:repositorio];
            break;
        }else{
            row+=[arrayRepositorios count];
        }
    }
    return [NSIndexPath indexPathForRow:row inSection:section];
}

-(NSInteger)numeroDeRepositoriosEmMemoria{
    NSInteger nRepositorios = 0;
    for(NSMutableArray * repositorios in self.paginasRepositorios){
        nRepositorios+=[repositorios count];
    }
    return nRepositorios;
}

-(NSInteger)numeroDePaginasEmMemoria{
    return _paginas;
}

-(NSInteger)numeroDeRepositoriosPorPagina{
    return per_page;
}

-(void)incrementarPaginaComFuncaoCallback:(void (^)(void))callback{
    _paginas++;
    NSMutableArray<Repositorio *> * novaPagina = [[NSMutableArray alloc] init];
    [self.paginasRepositorios addObject:novaPagina];
    [self repositoriosDaPagina:_paginas eCallback:callback];
}

#pragma mark Metodos privados de instancia
-(void)repositoriosDaPagina:(NSInteger)pagina eCallback:(void (^)(void))callbackFunction{
    NSString * urlAPI = [NSString stringWithFormat:@"%@&page=%ld&per_page=%ld",enderecoBaseAPI,(long)pagina,(long)per_page];
    AFHTTPSessionManager * sessionManager = [AFHTTPSessionManager manager];
    DCKeyValueObjectMapping * parser = [DCKeyValueObjectMapping mapperForClass:[Repositorio class] andConfiguration:self.configuracaoJsonParser];
    __weak NSMutableArray<Repositorio *> * arrayRepositorios = [self.paginasRepositorios objectAtIndex:pagina-1];
    NSMutableArray * novasCells = [[NSMutableArray alloc] init];

    [sessionManager GET:urlAPI parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary * repositorios = (NSDictionary *)responseObject;
        for(NSDictionary * repositorio in [repositorios valueForKey:@"items"]){
            NSMutableDictionary * mRepositorio = [NSMutableDictionary dictionaryWithDictionary:repositorio];
            Repositorio * novoRepositorio = [parser parseDictionary:mRepositorio];
            if([novoRepositorio.dono.tipoDoUsuario isEqualToString:@"User"]){
                [self setarNomeUsuarioDoDicionario:[mRepositorio valueForKey:@"owner"] noUsuario:novoRepositorio.dono];
            }else{
                [novoRepositorio.dono setNomeAutor:[NSString stringWithFormat:@"Organização %@",novoRepositorio.dono.username]];
            }
            [arrayRepositorios addObject:novoRepositorio];
            [novasCells addObject:[self indexDoRepositorio:novoRepositorio]];
        }
        
        [self.tableDelegate insertRowsAtIndexPaths:novasCells withRowAnimation:UITableViewRowAnimationNone];
        if(callbackFunction)callbackFunction();
    } failure:nil];
    void (^bloco)(NSInteger i,CGFloat x);
    bloco = ^(NSInteger i,CGFloat x){
        NSLog(@"oi");
    };
}




@end
