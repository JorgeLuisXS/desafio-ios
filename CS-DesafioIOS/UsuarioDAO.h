//
//  UsuarioDAO.h
//  DesafioIOSConcreteSolutions
//
//  Created by Luiz Cesar Lopes on 27/09/16.
//  Copyright © 2016 Jorge Luis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFHTTPSessionManager.h>
#import <DCKeyValueObjectMapping/DCParserConfiguration.h>
#import <DCKeyValueObjectMapping/DCObjectMapping.h>
#import <DCKeyValueObjectMapping/DCKeyValueObjectMapping.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "Usuario.h"
/*
 Essa classe, assim como qualquer Data Access Object padrao, serve apenas para encapsular o carregamento asincrono de dados de Usuario e permitir que tanto o RepositorioDAO como o PullRequestDAO tenha acesso a esses metodos encapsulados
 */

@interface UsuarioDAO : NSObject
-(void)popularImageView:(UIImageView *)imageView doUsuario:(Usuario *)usuario;
-(void)setarNomeUsuarioDoDicionario:(NSDictionary *)dono noUsuario:(Usuario *)usuario;
@property (weak,nonatomic) UITableView * tableDelegate;
@property (strong,nonatomic) DCParserConfiguration * configuracaoJsonParser;
@end
