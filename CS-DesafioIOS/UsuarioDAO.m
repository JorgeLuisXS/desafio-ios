//
//  UsuarioDAO.m
//  DesafioIOSConcreteSolutions
//
//  Created by Luiz Cesar Lopes on 27/09/16.
//  Copyright © 2016 Jorge Luis. All rights reserved.
//

#import "UsuarioDAO.h"

@implementation UsuarioDAO
-(id)init{
    self = [super init];
    if(self){
        self.configuracaoJsonParser = [DCParserConfiguration configuration];
        NSArray * configuracoes = @[
                                    [DCObjectMapping mapKeyPath:@"login" toAttribute:@"username" onClass:[Usuario class]],
                                    [DCObjectMapping mapKeyPath:@"type" toAttribute:@"tipoDoUsuario" onClass:[Usuario class]],
                                    [DCObjectMapping mapKeyPath:@"name" toAttribute:@"nomeAutor" onClass:[Usuario class]],
                                    [DCObjectMapping mapKeyPath:@"avatar_url" toAttribute:@"urlImagemDoUsuario" onClass:[Usuario class]]
                                    ];
        [self.configuracaoJsonParser addObjectMappings:configuracoes];
    }
    return self;
}

-(void)popularImageView:(UIImageView *)imageView doUsuario:(Usuario *)usuario{
    [imageView sd_setImageWithURL:[NSURL URLWithString:usuario.urlImagemDoUsuario]
                 placeholderImage: nil];
}

-(void)setarNomeUsuarioDoDicionario:(NSDictionary *)dono noUsuario:(Usuario *)usuario{
    NSString * urlAPI =[dono valueForKey:@"url"];
    NSString * url = [urlAPI stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

    AFHTTPSessionManager * sessionManager = [AFHTTPSessionManager manager];
    [sessionManager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary * userDict = responseObject;
        NSString * nomeDoAutor = [userDict valueForKey:@"name"];
        
        if(nomeDoAutor==NULL||nomeDoAutor==nil||[nomeDoAutor isEqual:[NSNull null]]){
            [usuario setNomeAutor:[NSString stringWithFormat:@"Usuario %@",usuario.username]];
        }else{
            [usuario setNomeAutor:nomeDoAutor];
        }
        [usuario.delegate nomeFoiCarregado:usuario.nomeAutor];
    } failure:nil];
}
@end
