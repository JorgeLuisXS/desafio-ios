//
//  RepositorioDAO.h
//  Estudo4
//
//  Created by Luiz Cesar Lopes on 27/09/16.
//  Copyright © 2016 Luis Cesar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Repositorio.h"
#import "UsuarioDAO.h"
/*
 O RepositorioDAO sera responsavel por criar e gerenciar o acesso a API e popular os repositorios,
 */
@interface RepositorioDAO : UsuarioDAO
//Uma colecao de colecoes de repositorios (cada item na colecao paginasRepositorios equivale a uma pagina de repositorios na API)
@property (strong,nonatomic,readonly) NSMutableArray<NSMutableArray <Repositorio *> *> * paginasRepositorios;

-(id)initWithTableDelegate:(UITableView *)tableDelegate;
-(Repositorio *)repositorioDoIndexPath:(NSIndexPath *)indexPath;
-(NSIndexPath *)indexDoRepositorio:(Repositorio *)repositorio;
-(NSInteger)numeroDePaginasEmMemoria;
-(NSInteger)numeroDeRepositoriosPorPagina;
-(NSInteger)numeroDeRepositoriosEmMemoria;
-(void)incrementarPaginaComFuncaoCallback:(void (^)(void))callback;
@end
