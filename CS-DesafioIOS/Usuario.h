//
//  Usuario.h
//  Estudo4
//
//  Created by Luiz Cesar Lopes on 27/09/16.
//  Copyright © 2016 Luis Cesar. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol UsuarioProtocol
-(void)nomeFoiCarregado:(NSString *)novoNome;//Se faz necessario pois o nome eh carregado assincronamente, devido a limitacao de so poder ser recuperado ao acessar a api de usuarios.
@end
@interface Usuario : NSObject
@property (strong,nonatomic) NSString * nomeAutor;//Se o usuario for do tipo Organizacao, sera 'Organizacao <username>'
@property (strong,nonatomic) NSString * username;//serve como id de identificacao
@property (strong,nonatomic) NSString * urlImagemDoUsuario;
@property (strong,nonatomic) NSString * tipoDoUsuario;
@property (weak,nonatomic) id<UsuarioProtocol> delegate;
@end
