//
//  PullRequest.h
//  Estudo4
//
//  Created by Luiz Cesar Lopes on 27/09/16.
//  Copyright © 2016 Luis Cesar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Usuario.h"
@interface PullRequest : NSObject
@property (strong,nonatomic) Usuario * criador;
@property (strong,nonatomic) NSString * paginaPullRequest;
@property (strong,nonatomic) NSString * tituloPullRequest;
@property (strong,nonatomic) NSString * corpoPullRequest;
@end
