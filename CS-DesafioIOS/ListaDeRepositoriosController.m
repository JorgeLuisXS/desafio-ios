//
//  ViewController.m
//  Estudo4
//
//  Created by Luiz Cesar Lopes on 26/09/16.
//  Copyright © 2016 Luis Cesar. All rights reserved.
//

/*
 Logica do Negocio:
 1- detectar quando scrollview atingir o chao
 2-
 */

#import "ListaDeRepositoriosController.h"
#import "RepositorioViewCell.h"
#import "RepositorioController.h"
@interface ListaDeRepositoriosController ()

@end
@implementation ListaDeRepositoriosController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.repositorioDAO = [[RepositorioDAO alloc] initWithTableDelegate:self.tableView];
    [self.repositorioDAO incrementarPaginaComFuncaoCallback:nil];

    [self.tableView registerNib:[UINib nibWithNibName:@"RepositorioViewCell" bundle:nil] forCellReuseIdentifier:@"reuse"];
    
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(popoverController)];
    backButton.tintColor = [UIColor whiteColor];
    self.navigationItem.backBarButtonItem = backButton;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger numberofRows = [self.repositorioDAO numeroDeRepositoriosEmMemoria];
    return numberofRows;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 146;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RepositorioViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuse"];
    if(!cell){
        cell = [[RepositorioViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reuse"];
    }
    // Configure the cell...
    __weak Repositorio * repositorioDesteIndex = [self.repositorioDAO repositorioDoIndexPath:indexPath];
    [cell popularComRepositorio:repositorioDesteIndex];
    [self.repositorioDAO popularImageView:cell.imagemUsuario doUsuario:repositorioDesteIndex.dono];
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row>=[self.repositorioDAO numeroDePaginasEmMemoria]*[self.repositorioDAO numeroDeRepositoriosPorPagina]*0.6){
        [self.repositorioDAO incrementarPaginaComFuncaoCallback:nil];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"mostrarRepositorio" sender:[self.repositorioDAO repositorioDoIndexPath:indexPath]];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    __weak Repositorio * repositorio = (Repositorio *) sender;
    __weak RepositorioController * viewController = [segue destinationViewController];
    [viewController.pullRequestDAO setarNomeUsuario:repositorio.dono.username eRepositorio:repositorio.nomeRepositorio];
    viewController.navigationItem.title = repositorio.nomeRepositorio;
    viewController.navigationItem.backBarButtonItem.tintColor = [UIColor whiteColor];
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 
 
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation

 */

@end
