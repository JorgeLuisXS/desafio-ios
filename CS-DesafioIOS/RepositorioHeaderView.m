//
//  RepositorioHeaderView.m
//  DesafioIOSConcreteSolutions
//
//  Created by Luiz Cesar Lopes on 27/09/16.
//  Copyright © 2016 Jorge Luis. All rights reserved.
//

#import "RepositorioHeaderView.h"

@implementation RepositorioHeaderView
-(id)init{
    self = [super init];
    if(self){
        self.backgroundColor = [UIColor whiteColor];
        self.label = [[UILabel alloc] init];
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.font = [UIFont boldSystemFontOfSize:15];
        _bottomBorder = [CALayer layer];
        _bottomBorder.backgroundColor = [UIColor grayColor].CGColor;
        [self.layer addSublayer:_bottomBorder];
        [self addSubview:self.label];
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    self.label.frame = self.frame;
    _bottomBorder.frame = CGRectMake(0, self.frame.size.height-1, self.frame.size.width, 1);
    self.label.adjustsFontSizeToFitWidth = YES;
}

-(void)setPullRequestsAbertas:(NSInteger)pullRequestsAbertas{
    _pullRequestsAbertas = pullRequestsAbertas;
    [self atualizarLabel];
}

-(void)setPullRequestsFechadas:(NSInteger)pullRequestsFechadas{
    _pullRequestsFechadas = pullRequestsFechadas;
    [self atualizarLabel];
}

-(void)atualizarLabel{
    NSString * pullsAbertas = [NSString stringWithFormat:@"%ld open",(long)_pullRequestsAbertas];
    NSString * pullsFechadas = [NSString stringWithFormat:@"%ld closed",(long)_pullRequestsFechadas];
    NSMutableAttributedString * text = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@/%@",pullsAbertas,pullsFechadas]];
    NSRange orangeRange = NSMakeRange(0,pullsAbertas.length);
    NSRange blackRange = NSMakeRange(pullsAbertas.length,pullsFechadas.length);
    [text addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:orangeRange];
    [text addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:blackRange];
    [self.label setAttributedText:text];
}

@end
