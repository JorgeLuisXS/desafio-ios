//
//  PullRequestDAO.m
////  Estudo4
//
//
//
//

#import "PullRequestDAO.h"
#define ABERTAS @"open"
#define FECHADAS @"closed"
static const NSString * urlPullAPI = @"https://api.github.com/search/issues?q=type:pr+repo:!REPLACE!&sort=created&‌​order=asc";
static const NSInteger  per_page=20;

@interface PullRequestDAO()
@property (assign) NSInteger paginas;
@end
@implementation PullRequestDAO
#pragma mark Metodos construtores
-(id)init{
    self = [super init];
    if(self){
        NSArray * configuracoes = @[
                                    [DCObjectMapping mapKeyPath:@"user" toAttribute:@"criador" onClass:[PullRequest class]],
                                    [DCObjectMapping mapKeyPath:@"html_url" toAttribute:@"paginaPullRequest" onClass:[PullRequest class]],
                                    [DCObjectMapping mapKeyPath:@"body" toAttribute:@"corpoPullRequest" onClass:[PullRequest class]],
                                    [DCObjectMapping mapKeyPath:@"title" toAttribute:@"tituloPullRequest" onClass:[PullRequest class]],
                                    ];
        [self.configuracaoJsonParser addObjectMappings:configuracoes];
        _paginasPullRequests = [[NSMutableArray alloc] init];
    }
    return self;
}

-(id)initWithPullDelegate:(id<PullRequestProtocol>) delegate{
    self = [self init];
    if(self){
        self.pullDelegate = delegate;
    }
    return self;
}

#pragma mark Metodos da instancia
-(void)setarNomeUsuario:(NSString *)username eRepositorio:(NSString *)reponame{
    _nomeUsuario = username;
    _nomeRepositorio = reponame;
    [self setarNumeroDePulls:ABERTAS];
    [self setarNumeroDePulls:FECHADAS];
}
-(NSInteger)numeroDePullrequestsEmMemoria{
    NSInteger nPullrequests = 0;
    for(NSMutableArray * pulls in self.paginasPullRequests){
        nPullrequests+=[pulls count];
    }
    return nPullrequests;
}

-(PullRequest *)pullRequestDoIndexPath:(NSIndexPath *)indexPath{
    NSInteger index = indexPath.row%per_page;
    NSInteger page = indexPath.row/per_page;
    __weak NSMutableArray * pullRequests = [self.paginasPullRequests objectAtIndex:page];
    __weak PullRequest * pullRequest;
    pullRequest =[pullRequests objectAtIndex:index];
    return pullRequest;
}

-(NSIndexPath *)indexPathDoPullRequest:(PullRequest *)pullRequest{
    NSInteger row=0;
    NSInteger section=0;
    for(NSMutableArray * arraypullRequests in self.paginasPullRequests){
        if([arraypullRequests containsObject:pullRequest]){
            row+= [arraypullRequests indexOfObject:pullRequest];
            break;
        }else{
            row+=[arraypullRequests count];
        }
    }
    return [NSIndexPath indexPathForRow:row inSection:section];
}

-(NSInteger)numeroDePaginasEmMemoria{
    return _paginas;
}

-(NSInteger)numeroDePullrequestsPorPagina{
    return per_page;
}

-(BOOL)incrementarPaginaComFuncaoCallback:(void (^)(void))callback{
    if(!self.nomeUsuario||!self.nomeRepositorio)return NO;
    _paginas++;
    NSMutableArray<PullRequest *> * novaPagina = [[NSMutableArray alloc] init];
    [self.paginasPullRequests addObject:novaPagina];
    [self pullRequestsDaPagina:_paginas eCallback:callback];
    return YES;
}

#pragma mark Metodos privados da instancia
-(void)pullRequestsDaPagina:(NSInteger)pagina eCallback:(void (^)(void))callbackFunction{
    NSString * repositorioDosPulls = [NSString stringWithFormat:@"%@/%@",self.nomeUsuario,self.nomeRepositorio];
    NSString * preparingBaseURL = [urlPullAPI stringByReplacingOccurrencesOfString:@"!REPLACE!" withString:repositorioDosPulls];
    NSString * baseUrl = [NSString stringWithFormat:@"%@&page=%ld&per_page=%ld",preparingBaseURL,(long)pagina,(long)per_page];
    NSString * url = [baseUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    AFHTTPSessionManager * sessionManager = [AFHTTPSessionManager manager];
    DCKeyValueObjectMapping * parser = [DCKeyValueObjectMapping mapperForClass:[PullRequest class] andConfiguration:self.configuracaoJsonParser];

    
    __weak NSMutableArray<PullRequest *> * arrayPullrequests= [self.paginasPullRequests objectAtIndex:pagina-1];
    NSMutableArray * novasCells = [[NSMutableArray alloc] init];
    
    [sessionManager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary * pulls = (NSDictionary *)responseObject;
        for(NSDictionary * pull in [pulls valueForKey:@"items"]){
            NSMutableDictionary * mPull = [NSMutableDictionary dictionaryWithDictionary:pull];
            PullRequest * novoPullRequest = [parser parseDictionary:mPull];
            if([novoPullRequest.criador.tipoDoUsuario isEqualToString:@"User"]){
                [self setarNomeUsuarioDoDicionario:[mPull valueForKey:@"user"] noUsuario:novoPullRequest.criador];
            }else{
                [novoPullRequest.criador setNomeAutor:[NSString stringWithFormat:@"Organização %@",novoPullRequest.criador.username]];
            }
            [arrayPullrequests addObject:novoPullRequest];
            [novasCells addObject:[self indexPathDoPullRequest:novoPullRequest]];
        }
        [self.tableDelegate insertRowsAtIndexPaths:novasCells withRowAnimation:UITableViewRowAnimationNone];
        if(callbackFunction)callbackFunction();
    } failure:nil];
}

-(void)setarNumeroDePulls:(NSString *)closedOrOpen{
    NSString * baseURL = [NSString stringWithFormat:@"https://api.github.com/search/issues?q=+type:pr+state:%@+repo:%@/%@&sort=created&‌​order=asc&page=1&per_page=1",closedOrOpen,self.nomeUsuario,self.nomeRepositorio];
    NSString * url = [baseURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    AFHTTPSessionManager * sessionManager = [AFHTTPSessionManager manager];
    [sessionManager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary * itens = (NSDictionary *)responseObject;
        NSDictionary * response = itens;
        if([closedOrOpen isEqualToString:FECHADAS]){
            NSInteger closedPulls = [[response valueForKey:@"total_count"] integerValue];
            [self.pullDelegate numeroDePullsFechadasAtualizado:closedPulls];
        }
        else if([closedOrOpen isEqualToString:ABERTAS]){
             NSInteger openedPulls = [[response valueForKey:@"total_count"] integerValue];
            [self.pullDelegate numeroDePullsAbertasAtualizado:openedPulls];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Verifique sua conexao, caso ela esteja funcionando, o limite de requests permitido pelo GitHub foi atingido, os dados podem nao carregar adequadamente");
    }];
}

@end
