//
//  ViewController.h
//  Estudo4
//
//  Created by Luiz Cesar Lopes on 26/09/16.
//  Copyright © 2016 Luis Cesar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepositorioDAO.h"
@interface ListaDeRepositoriosController : UITableViewController
@property (strong,nonatomic) RepositorioDAO * repositorioDAO;

@end

