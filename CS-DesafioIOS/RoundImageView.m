//
//  RoundImageView.m
//  Estudo4
//
//  Created by Luiz Cesar Lopes on 27/09/16.
//  Copyright © 2016 Luis Cesar. All rights reserved.
//

#import "RoundImageView.h"

@implementation RoundImageView
//Garantindo que o ImageView terao lados iguais para seja um circulo perfeito
-(void)arredondar{
    self.layer.cornerRadius = self.frame.size.width/2;
    self.clipsToBounds = YES;
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
