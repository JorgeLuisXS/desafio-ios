//
//  AppDelegate.h
//  CS-DesafioIOS
//
//  Created by Luiz Cesar Lopes on 27/09/16.
//  Copyright © 2016 Jorge Luis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

