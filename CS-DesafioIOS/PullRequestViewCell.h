//
//  PullRequestViewCell.h
//  Estudo4
//
//  Created by Luiz Cesar Lopes on 26/09/16.
//  Copyright © 2016 Luis Cesar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullRequest.h"
#import "RoundImageView.h"
#import "FirstCapLabel.h"
@interface PullRequestViewCell : UITableViewCell <UsuarioProtocol>
@property (strong, nonatomic) IBOutlet FirstCapLabel *tituloPullRequest;
@property (strong, nonatomic) IBOutlet FirstCapLabel *bodyPullRequest;
@property (strong, nonatomic) IBOutlet RoundImageView *imagemUsuario;
@property (strong, nonatomic) IBOutlet UILabel *username;
@property (strong, nonatomic) IBOutlet FirstCapLabel *nomeCompletoUsuario;
@property (weak, nonatomic) PullRequest * pullRequestDestaCell;
-(void)popularComPullRequest:(PullRequest *)pullRequest;
@end
