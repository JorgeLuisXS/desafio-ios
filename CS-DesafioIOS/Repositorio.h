//
//  Repositorio.h
//  Estudo4
//
//  Created by Luiz Cesar Lopes on 27/09/16.
//  Copyright © 2016 Luis Cesar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Usuario.h"
@interface Repositorio : NSObject
@property (strong,nonatomic) Usuario * dono;
@property (strong,nonatomic) NSString * nomeRepositorio;
@property (strong,nonatomic) NSString * descricaoRepositorio;
@property (strong,nonatomic) NSNumber * estrelas;
@property (strong,nonatomic) NSNumber * forks;
@end
