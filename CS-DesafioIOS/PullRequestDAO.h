//
//  PullRequestDAO.h
//  Estudo4
//
//  Created by Luiz Cesar Lopes on 27/09/16.
//  Copyright © 2016 Luis Cesar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UsuarioDAO.h"
#import "PullRequest.h"
#import "RepositorioHeaderView.h"

@protocol PullRequestProtocol
-(void)numeroDePullsAbertasAtualizado:(NSInteger)openPulls;
-(void)numeroDePullsFechadasAtualizado:(NSInteger)closedPulls;
@end

@interface PullRequestDAO : UsuarioDAO
@property (strong,nonatomic,readonly) NSMutableArray<NSMutableArray<PullRequest *> *> * paginasPullRequests;
@property (weak,readonly,nonatomic) NSString * nomeRepositorio;
@property (weak,readonly,nonatomic) NSString * nomeUsuario;
@property (weak,nonatomic) id<PullRequestProtocol> pullDelegate;

-(id)initWithPullDelegate:(id<PullRequestProtocol>) delegate;
-(PullRequest *)pullRequestDoIndexPath:(NSIndexPath *)indexPath;
-(NSIndexPath *)indexPathDoPullRequest:(PullRequest *)pullRequest;
-(NSInteger)numeroDePaginasEmMemoria;
-(NSInteger)numeroDePullrequestsPorPagina;
-(NSInteger)numeroDePullrequestsEmMemoria;
-(BOOL)incrementarPaginaComFuncaoCallback:(void (^)(void))callback;
-(void)setarNomeUsuario:(NSString *)username eRepositorio:(NSString *)reponame;
@end
