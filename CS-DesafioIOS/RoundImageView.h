//
//  RoundImageView.h
//  Estudo4
//
//  Created by Luiz Cesar Lopes on 27/09/16.
//  Copyright © 2016 Luis Cesar. All rights reserved.
//

#import <UIKit/UIImageView.h>
#import <QuartzCore/QuartzCore.h>
//UIImageView perfeitamente redonda que sera usada nas cells de Repositorio e Pullrequest.
@interface RoundImageView : UIImageView
-(void)arredondar;
@end
