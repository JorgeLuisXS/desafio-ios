//
//  FirstCapLabel.h
//  CS-DesafioIOS
//
//  Created by Luiz Cesar Lopes on 29/09/16.
//  Copyright © 2016 Jorge Luis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstCapLabel : UILabel
+(CGFloat)fontToFitWidthOf:(UILabel *)label; //Faz o texto do label ficar do tamanho correto, sem permitir truncagem
@end
