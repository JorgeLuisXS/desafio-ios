//
//  RepositorioHeaderView.h
//  Estudo4
//
//  Created by Luiz Cesar Lopes on 27/09/16.
//  Copyright © 2016 Luis Cesar. All rights reserved.
//

#import <UIKit/UIView.h>
#import <UIKit/UILabel.h>
#import <UIKit/NSAttributedString.h>
#import <QuartzCore/CALayer.h>
//Classe UIView que irá encapsular a visualizacao da relacao de Pull requests abertas e fechadas
@interface RepositorioHeaderView : UIView
@property (assign,nonatomic) NSInteger pullRequestsAbertas;
@property (assign,nonatomic) NSInteger pullRequestsFechadas;
@property (strong,readonly) CALayer * bottomBorder;
@property (strong,nonatomic) UILabel * label;
@end
