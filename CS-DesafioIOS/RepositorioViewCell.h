//
//  RepositorioViewCell.h
//  Estudo4
//
//  Created by Luiz Cesar Lopes on 26/09/16.
//  Copyright © 2016 Luis Cesar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Repositorio.h"
#import "RoundImageView.h"
#import "FirstCapLabel.h"
@interface RepositorioViewCell : UITableViewCell <UsuarioProtocol>
@property (strong, nonatomic) IBOutlet FirstCapLabel *nomeRepositorio;
@property (strong, nonatomic) IBOutlet FirstCapLabel *descricaoRepositorio;
@property (strong, nonatomic) IBOutlet RoundImageView *imagemUsuario;
@property (strong, nonatomic) IBOutlet UILabel *loginUsuario;
@property (strong, nonatomic) IBOutlet FirstCapLabel *nomeUsuario;
@property (strong, nonatomic) IBOutlet UILabel *estrelas;
@property (strong, nonatomic) IBOutlet UILabel *forks;
@property (weak,nonatomic) Repositorio * repositorioDestaCell;
-(void)popularComRepositorio:(Repositorio *)repositorio;
@end
