//
//  RepositorioController.h
//  Estudo4
//
//  Created by Luiz Cesar Lopes on 26/09/16.
//  Copyright © 2016 Luis Cesar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullRequestDAO.h"

@interface RepositorioController : UITableViewController <PullRequestProtocol>
@property (strong,nonatomic) RepositorioHeaderView * headerView;
@property (strong,nonatomic) PullRequestDAO * pullRequestDAO;
@end
