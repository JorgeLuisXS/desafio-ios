//
//  RepositorioViewCell.m
//  Estudo4
//
//  Created by Luiz Cesar Lopes on 26/09/16.
//  Copyright © 2016 Luis Cesar. All rights reserved.
//

#import "RepositorioViewCell.h"

@implementation RepositorioViewCell

-(void)layoutSubviews{
    [super layoutSubviews];
    self.nomeUsuario.adjustsFontSizeToFitWidth = YES;
    self.nomeRepositorio.adjustsFontSizeToFitWidth =YES;
    if([self.repositorioDestaCell.dono.tipoDoUsuario isEqualToString:@"User"])[self.imagemUsuario arredondar];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)nomeFoiCarregado:(NSString *)novoNome{
    self.nomeUsuario.text = novoNome;
}
-(void)popularComRepositorio:(Repositorio *)repositorio{
    self.repositorioDestaCell = repositorio;
    self.repositorioDestaCell.dono.delegate = self;
    self.estrelas.text = [repositorio.estrelas stringValue];
    self.forks.text = [repositorio.forks stringValue];
    self.loginUsuario.text = repositorio.dono.username;
    self.nomeUsuario.text = repositorio.dono.nomeAutor;
    self.nomeRepositorio.text = repositorio.nomeRepositorio;
    self.descricaoRepositorio.text = repositorio.descricaoRepositorio;
}

@end
