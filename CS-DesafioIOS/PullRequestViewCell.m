//
//  PullRequestViewCell.m
//  Estudo4
//
//  Created by Luiz Cesar Lopes on 26/09/16.
//  Copyright © 2016 Luis Cesar. All rights reserved.
//

#import "PullRequestViewCell.h"

@implementation PullRequestViewCell
-(void)layoutSubviews{
    [super layoutSubviews];
    if([self.pullRequestDestaCell.criador.tipoDoUsuario isEqualToString:@"User"])[self.imagemUsuario arredondar];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)nomeFoiCarregado:(NSString *)novoNome{
    self.nomeCompletoUsuario.text = novoNome ;
}
-(void)popularComPullRequest:(PullRequest *)pullRequest{
    self.pullRequestDestaCell = pullRequest;
    pullRequest.criador.delegate = self;
    self.nomeCompletoUsuario.text = pullRequest.criador.nomeAutor==NULL||pullRequest.criador.nomeAutor==nil?@"":pullRequest.criador.nomeAutor;
    self.tituloPullRequest.text = pullRequest.tituloPullRequest ;
    self.bodyPullRequest.text = pullRequest.corpoPullRequest==nil||pullRequest.corpoPullRequest==NULL||[pullRequest.corpoPullRequest length]==0?@"No message":pullRequest.corpoPullRequest ;
    self.username.text = pullRequest.criador.username ;
}

@end
