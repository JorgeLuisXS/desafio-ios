//
//  RepositorioController.m
//  Estudo4
//
//  Created by Luiz Cesar Lopes on 26/09/16.
//  Copyright © 2016 Luis Cesar. All rights reserved.
//
#import "RepositorioController.h"
#import "PullRequestViewCell.h"
@interface RepositorioController ()

@end

@implementation RepositorioController

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        self.pullRequestDAO = [[PullRequestDAO alloc] initWithPullDelegate:self];
        self.headerView = [[RepositorioHeaderView alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pullRequestDAO.tableDelegate = self.tableView;
    [self.pullRequestDAO incrementarPaginaComFuncaoCallback:nil];
    [self.tableView registerNib:[UINib nibWithNibName:@"PullRequestViewCell" bundle:nil] forCellReuseIdentifier:@"reuse"];
    UILabel * label = [[UILabel alloc] initWithFrame:self.navigationController.navigationBar.frame]; //CGRectMake(0, 0, self.view.frame.size.width,  self.navigationController.navigationBar.frame.size.height)];
    label.text = self.navigationItem.title;
    label.font = [UIFont fontWithName:@"Verdana" size:27];
    [FirstCapLabel fontToFitWidthOf:label];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys: label.font,NSFontAttributeName, [UIColor whiteColor],NSForegroundColorAttributeName, nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.pullRequestDAO numeroDePullrequestsEmMemoria];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 139;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return self.headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PullRequestViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuse" forIndexPath:indexPath];
    if(!cell){
        cell = [[PullRequestViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reuse"];
    }
    __weak PullRequest * pullRequest = [self.pullRequestDAO pullRequestDoIndexPath:indexPath];
    [cell popularComPullRequest:pullRequest];
    [self.pullRequestDAO popularImageView:cell.imagemUsuario doUsuario:pullRequest.criador];
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row>=[self.pullRequestDAO numeroDePaginasEmMemoria]*[self.pullRequestDAO numeroDePullrequestsPorPagina]*0.6){
        [self.pullRequestDAO incrementarPaginaComFuncaoCallback:nil];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    __weak PullRequest * pullRequestSelecionada = [self.pullRequestDAO pullRequestDoIndexPath:indexPath];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:pullRequestSelecionada.paginaPullRequest]];
}

-(void)numeroDePullsAbertasAtualizado:(NSInteger)openPulls{
    self.headerView.pullRequestsAbertas = openPulls;
}
-(void)numeroDePullsFechadasAtualizado:(NSInteger)closedPulls{
        self.headerView.pullRequestsFechadas= closedPulls;
}

@end
