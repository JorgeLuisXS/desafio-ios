//
//  FirstCapLabel.m
//  CS-DesafioIOS
//
//  Created by Luiz Cesar Lopes on 29/09/16.
//  Copyright © 2016 Jorge Luis. All rights reserved.
//

#import "FirstCapLabel.h"

@implementation FirstCapLabel

-(void)setText:(NSString *)text{
    if([text length]>=1){
        [super setText:[NSString stringWithFormat:@"%@%@",[[text substringToIndex:1] uppercaseString],[text substringFromIndex:1]]];
    }else{
        [super setText:@" "];
    }
}

+(CGFloat)fontToFitWidthOf:(UILabel *)label{
    CGRect frame2 = [label.text boundingRectWithSize:CGSizeMake(30000.0f, 30000.0f) options:NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[label.font fontWithSize:1]} context:nil];
    CGSize size2= frame2.size;
    label.font = [label.font fontWithSize:label.frame.size.width/(size2.width*1.3)];
    CGRect frame3 = [label.text boundingRectWithSize:CGSizeMake(30000.0f, 30000.0f) options:NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:label.font} context:nil];
    CGSize size3= frame3.size;
    while((size3.height*1.3)>=label.frame.size.height){
        label.font = [label.font fontWithSize:label.font.pointSize-1];
        frame3 = [label.text boundingRectWithSize:CGSizeMake(30000.0f, 30000.0f) options:NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:label.font} context:nil];
        size3= frame3.size;
    }
    return 0;
}
@end
